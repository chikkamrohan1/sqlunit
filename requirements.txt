cx-Oracle==8.3.0
Flask==2.1.2
flask-restx==0.5.1
Jinja2==3.1.2
pandas==1.4.3
pytest==7.1.2
PyYAML==6.0
regex==2022.7.9
typer==0.5.0
hvac==0.11.2