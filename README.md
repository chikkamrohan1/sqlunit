# SQL Unit

A wrapper to automate unit tests for SQL using pytest

View full docs [here](https://chikkamrohan.gitbook.io/sql-unit-docs)

## Getting started

Clone this repository and build the tool using the provided tests, using the following commands:
``` bash
git clone https://gitlab.com/chikkamrohan1/sqlunit.git
cd sqlunit
pip install -r requirements.txt

python generator.py generate    #Produces unit tests from example test configuration
cd src/
python app.py   #Launches SwaggerUI for easy usage of test cases (default: http://localhost:5000)
```