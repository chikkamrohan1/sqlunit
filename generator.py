# ==========================================
# Author: Rohan Chikkam
# Date:   28 Jul 2022
# ==========================================

import typer
import os
import re
import shutil
import utils
import pytest
import yaml
import hvac
from jinja2 import Template

app = typer.Typer()  # Used for pretty CLI tool
tests = [x for x in os.walk("tests")][0][1]  # Find all tests
routines = [x[0:-4] for x in [x for x in os.walk("routines")][-1][-1]]


@app.command()
def validate():
    """
    Validate all yml config files

    Checks all yml configs, presence of source data files, and attempts to validate SQL queries
    """

    pytest.main(["-s", "generator.py"])


@app.command()
def generate(
    force: bool = typer.Option(
        False,
        help="Force generation of unit tests without validation",
    )
):
    """
    Generate unit tests from yml config files

    Use --force to skip validation
    """
    if not force:  # Skip validation if --force is used
        validate()
    for test in tests:
        generate_test(test)

    if routines != None:
        for routine in routines:
            generate_routine(routine)
        generate_api(tests + routines)  # Add generated tests to API definition

    else:
        generate_api(tests)


@pytest.mark.parametrize("test", tests)
def test_config(test):

    print("\nTesting config for test: {}".format(test))

    test_files = [x for x in os.walk("tests/{}".format(test))][0][
        2
    ]  # Find dependant test files

    assert (
        "config.yml" in test_files
    ), "Test case {} has missing config.yml file".format(
        test
    )  # Check config exists

    testResult = utils.validate_config(
        "tests/{}/config.yml".format(test)
    )  # Check basic configurationd details are valid

    assert (
        testResult.test_Name()
    ), "Name provided in YML config for {} is missing or invalid".format(
        test
    )  # Check a name has been provided for the unit test
    assert (
        testResult.test_ConnDetails()
    ), "SQL connections details provided in YML config for {} is missing or invalid".format(
        test
    )  # Check a SQL connection details are provided


@pytest.mark.parametrize("routine", routines)
def test_routine(routine):
    print("\nTesting config for routine: {}".format(routine))
    testResult = utils.validate_config("routines/{}.yml".format(routine))
    assert (
        testResult.test_Name()
    ), "Name provided in YML routine for {} is missing or invalid".format(routine)

    with open("routines/{}.yml".format(routine), "r") as routine_file:
        routine_data = yaml.safe_load(routine_file)

    for test in routine_data["tests"]:
        assert test in tests, "Test {} specified in routine {} does not exist".format(
            test, routine
        )


def generate_test(test):
    print("Found definition for {}".format(test))

    with open("tests/{}/config.yml".format(test), "r") as config_file:  # Read config
        config_data = yaml.safe_load(config_file)

    try:  # Attempt load creds from keyvault
        client = hvac.Client(url=config_data["kv"], token=config_data["token"])
        create_response = client.secrets.kv.v2.read_secret_version(
            path=config_data["path"]
        )["data"]["data"]
        username = create_response["user"]
        password = create_response["password"]
        host = create_response["host"]
        dbname = create_response["dbname"]
    except:
        username = config_data["user"]
        password = config_data["pass"]
        host = config_data["host"]
        dbname = config_data["db"]

        pass

    setup_files = config_data["setup"]  # Files to use before test
    query_files = config_data["query"]  # Queries to test
    teardown_files = config_data["teardown"]  # Queries to run after test
    result_file = config_data["result"]  # Expected console output (Optional)

    eval_files = []
    all_files = []

    if setup_files != None:
        eval_files = [
            x for x in setup_files if re.compile("^expected_.*[.]csv$").match(x)
        ]  # Files used for evaulation of unit test
        all_files += setup_files
    if query_files != None:
        all_files += query_files
    if teardown_files != None:
        all_files += teardown_files
    if result_file != None:
        all_files.append(result_file)

    print("Config specified for: {}".format(all_files))
    print("Setup using: {}".format(setup_files))
    print("Test using: {}".format(query_files))
    print("Teardown using: {}".format(teardown_files))
    print("Console output evaluation using: {}".format(result_file))

    try:
        os.mkdir("src/{}".format(test))  # Create output directory for test
    except:
        print(
            "Warning! Problem encountered creating folder src/{} (Folder may already exists)".format(
                test
            )
        )

    for file in all_files:  # Copy dependant files to build directory
        shutil.copyfile(
            "tests/{}/{}".format(test, file), "src/{}/{}".format(test, file)
        )

    # Generate unit test using Jinja template
    data = open("templates/test_template.jinja", "r")
    with data:
        template = Template(data.read())
        template_result = template.render(
            config_name=config_data["name"],
            config_desc=config_data["description"],
            test_name=test,
            host=host,
            db=dbname,
            user=username,
            password=password,
            port=config_data["port"],
            rollback_on_error=config_data["rollback_on_error"],
            continue_on_error=config_data["continue_on_error"],
            setup=setup_files,
            query=query_files,
            teardown=teardown_files,
            eval=eval_files,
            result=result_file,
        )
    with open("src/{}/config.py".format(test), "w") as template_file:  # Write unit test
        template_file.write(template_result)

    w = open("src/{}/__init__.py".format(test), "w")

    print("\n")


def generate_routine(routine):
    print("Found definition for {}".format(routine))

    with open("routines/{}.yml".format(routine), "r") as routine_file:
        routine_data = yaml.safe_load(routine_file)

    try:
        # Create output directory for routine
        os.mkdir("src/{}".format(routine))
    except:
        print(
            "Warning! Problem encountered creating folder src/{} (Folder may already exists)".format(
                routine
            )
        )

    # Generate routine using Jinja template
    data = open("templates/routine_template.jinja", "r")
    with data:
        template = Template(data.read())
        template_result = template.render(
            config_name=routine_data["name"],
            config_desc=routine_data["description"],
            routines=routine_data["tests"],
        )
    with open(
        "src/{}/config.py".format(routine), "w"
    ) as template_file:  # Write unit test
        template_file.write(template_result)

    w = open("src/{}/__init__.py".format(routine), "w")
    pass


def generate_api(tests):
    # Generate API definition using Jinja template
    data = open("templates/api_template.jinja", "r")
    with data:
        template = Template(data.read())
        template_result = template.render(apis=tests)
    with open("src/apis.py", "w") as template_file:  # Write API definition
        template_file.write(template_result)


if __name__ == "__main__":
    app()
